var time_drift = 0;
var delta_offset = 0;
var server_time = 0;
$(function() {
	function init_clock() {
		$.get("https://www.eng.uwaterloo.ca/~enginfo/api/time", function(data) {
			var server = new Date(data.now);
			var local = new Date();
			var server_time = server.getTime();
			var local_time = local.getTime();
			var server_offset = data.offset;
			var local_offset = local.getTimezoneOffset();
			time_drift = server_time - local_time;
			delta_offset = local_offset + server_offset;
			// initializes the time difference to account for time drift on remote machines
			// and in case of some weirdness with timezones, use the server's timezone
		});
	}

	function update_clock() {
		var local = new Date();
		var fixed_local = new Date(d.getTime() + time_drift + delta_offset * 60 * 1000);
		console.log(fixed_local.toLocaleTimeString('en-US', { hour: '2-digit', minute: '2-digit' }));
		console.log(fixed_local.toLocaleDateString('en-US', {weekday: 'long'}));
		console.log(fixed_local.toLocaleDateString('en-US', {month: 'long'}));
		console.log(fixed_local.toLocaleDateString('en-US', {day: 'numeric'}));
		console.log(fixed_local.toLocaleDateString('en-US', {year: 'numeric'}));
	}
	setInterval(function() {
		update_clock();
	}, 1000);

	init_clock();
});